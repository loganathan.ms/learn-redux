# Learn Redux

A simple React + Redux implementation. This will be turned into a free video series once the app is totally fleshed out.

## Running

First `npm install` to grab all the necessary dependencies. 

Then run `npm start` and open <localhost:7770> in your browser.

## Production Build

Run `npm build` to create a distro folder and a bundle.js file.

## Useful Links

 - New `react-router-redux`: [https://github.com/ReactTraining/react-router/tree/master/packages/react-router-redux](https://github.com/ReactTraining/react-router/tree/master/packages/react-router-redux)
 - Pass down props to routes: [https://github.com/ReactTraining/react-router/issues/4105#issuecomment-289195202](https://github.com/ReactTraining/react-router/issues/4105#issuecomment-289195202)
 - Migrating `react-addons-css-transition-group` to `react-transition-group`: [https://github.com/reactjs/react-transition-group/blob/master/Migration.md](https://github.com/reactjs/react-transition-group/blob/master/Migration.md)
